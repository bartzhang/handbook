---
title: "Engineering Career Framework: Intern"
---

## Engineering Function Competencies: Intern
 
**Interns at GitLab are expected to exhibit the following competencies:**

- [Intern Leadership Competencies](#intern-leadership-competencies)
- [Intern Technical Competencies](#intern-technical-competencies)
- [Intern Values Alignment](#intern-values-alignment)

---

### Intern Leadership Competencies

{{% include "includes/engineering/intern-leadership-competency.md" %}}
  
### Intern Technical Competencies

{{% include "includes/engineering/intern-technical-competency.md" %}}

### Intern Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
